/*
HBP HEADER!?
*/

#ifndef BTSOFTPLANECOLLISIONALGORITHM_H
#define BTSOFTPLANECOLLISIONALGORITHM_H


#include "BulletCollision/BroadphaseCollision/btCollisionAlgorithm.h"
#include "BulletCollision/BroadphaseCollision/btBroadphaseProxy.h"
#include "BulletCollision/CollisionDispatch/btCollisionCreateFunc.h"
class btPersistentManifold;
#include "BulletCollision/CollisionDispatch/btCollisionDispatcher.h"

#include "LinearMath/btVector3.h"
class btSoftBody;

class btSoftPlaneCollisionAlgorithm : public btCollisionAlgorithm
{
    //	bool	m_ownManifold;
    //	btPersistentManifold*	m_manifoldPtr;

    btSoftBody*				m_softBody;
    btCollisionObject*		m_rigidCollisionObject;

    ///for plane versus soft (instead of soft versus plane), we use this swapped boolean
    bool	m_isSwapped;

public:

    btSoftPlaneCollisionAlgorithm(btPersistentManifold* mf,const btCollisionAlgorithmConstructionInfo& ci,const btCollisionObjectWrapper* col0,const btCollisionObjectWrapper* col1Wrap, bool isSwapped);

    virtual ~btSoftPlaneCollisionAlgorithm();

    virtual void processCollision (const btCollisionObjectWrapper* body0Wrap,const btCollisionObjectWrapper* body1Wrap,const btDispatcherInfo& dispatchInfo,btManifoldResult* resultOut);

    virtual btScalar calculateTimeOfImpact(btCollisionObject* body0,btCollisionObject* body1,const btDispatcherInfo& dispatchInfo,btManifoldResult* resultOut);

    virtual	void	getAllContactManifolds(btManifoldArray&	manifoldArray);

    virtual const char* getAlgorithmName() { return "Soft-body-Plane collision"; }

    struct CreateFunc :public 	btCollisionAlgorithmCreateFunc
    {
        virtual	btCollisionAlgorithm* CreateCollisionAlgorithm(btCollisionAlgorithmConstructionInfo& ci, const btCollisionObjectWrapper* body0Wrap,const btCollisionObjectWrapper* body1Wrap)
        {
            void* mem = ci.m_dispatcher1->allocateCollisionAlgorithm(sizeof(btSoftPlaneCollisionAlgorithm));
            if (!m_swapped)
            {
                return new(mem) btSoftPlaneCollisionAlgorithm(0,ci,body0Wrap,body1Wrap,false);
            } else
            {
                return new(mem) btSoftPlaneCollisionAlgorithm(0,ci,body0Wrap,body1Wrap,true);
            }
        }
    };
};

#endif // BTSOFTPLANECOLLISIONALGORITHM_H
