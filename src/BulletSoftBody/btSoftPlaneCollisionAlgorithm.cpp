#include "btSoftPlaneCollisionAlgorithm.h"
#include "BulletCollision/CollisionDispatch/btCollisionDispatcher.h"
#include "BulletCollision/CollisionShapes/btSphereShape.h"
#include "BulletCollision/CollisionShapes/btBoxShape.h"
#include "BulletCollision/CollisionDispatch/btCollisionObject.h"
#include "btSoftBody.h"
#include "BulletSoftBody/btSoftBodySolvers.h"
#include "BulletCollision/CollisionDispatch/btCollisionObjectWrapper.h"

btSoftPlaneCollisionAlgorithm::btSoftPlaneCollisionAlgorithm(btPersistentManifold *mf, const btCollisionAlgorithmConstructionInfo &ci, const btCollisionObjectWrapper *col0, const btCollisionObjectWrapper *col1Wrap, bool isSwapped)
    : btCollisionAlgorithm(ci)
    //, m_ownManifold(false)
    //, m_manifoldPtr(mf)
    , m_isSwapped(isSwapped)
{
}

btSoftPlaneCollisionAlgorithm::~btSoftPlaneCollisionAlgorithm()
{

}

void btSoftPlaneCollisionAlgorithm::processCollision(const btCollisionObjectWrapper *body0Wrap, const btCollisionObjectWrapper *body1Wrap, const btDispatcherInfo &dispatchInfo, btManifoldResult *resultOut)
{
    printf("PLANE processCollision(): %s and %s\n", body0Wrap->getCollisionShape()->getName(), body1Wrap->getCollisionShape()->getName());

    btSoftBody* softBody =  m_isSwapped? (btSoftBody*)body1Wrap->getCollisionObject() : (btSoftBody*)body0Wrap->getCollisionObject();
    const btCollisionObjectWrapper* plane = m_isSwapped? body0Wrap : body1Wrap;

    printf("Plane: %s\n", plane->getCollisionShape()->getName());

    //btBoxShape *groundShape = new btBoxShape(btVector3(100000,5,100000));
    //groundShape->setMargin(0.5);
    //btCollisionObject* colObj0 = (btCollisionObject*)groundShape;
    //btCollisionObjectWrapper wrapPlane(0,colObj0->getCollisionShape(),colObj0,colObj0->getWorldTransform(),-1,-1);

    if (softBody->m_collisionDisabledObjects.findLinearSearch(plane->getCollisionObject())==softBody->m_collisionDisabledObjects.size())
    {
        softBody->getSoftBodySolver()->processCollision(softBody, plane);
    }
    //delete groundShape;


#if 0
    { // Taken from btGImpactCollisionAlgorithm::gimpacttrimeshpart_vs_plane_collision
    btTransform orgtrans0 = body0Wrap->getWorldTransform();
    btTransform orgtrans1 = body1Wrap->getWorldTransform();

    const btPlaneShape * planeshape = static_cast<const btPlaneShape *>(shape1);
    btVector4 plane;
    planeshape->get_plane_equation_transformed(orgtrans1,plane);

    //test box against plane

    btAABB tribox;
    shape0->getAabb(orgtrans0,tribox.m_min,tribox.m_max);
    tribox.increment_margin(planeshape->getMargin());

    if( tribox.plane_classify(plane)!= BT_CONST_COLLIDE_PLANE) return;

    shape0->lockChildShapes();

    btScalar margin = shape0->getMargin() + planeshape->getMargin();

    btVector3 vertex;
    int vi = shape0->getVertexCount();

    btScalar epsilon = 1e-03;

    // printf("  vertex count = %i; test with margin = %f\n", vi, margin);
    int contactsAdded = 0;
    while(vi--)
    {
        shape0->getVertex(vi,vertex);
        vertex = orgtrans0(vertex);

        btScalar distance = vertex.dot(plane) - plane[3] - margin;

        // printf("  vertex %i = %f,%f,%f: distance = %f\n", vi, vertex.x(), vertex.y(), vertex.z(), distance);

        if(distance <= epsilon /*< 0.0*/)//add contact
        {
            ++contactsAdded;
            if(swapped)
            {
                // printf("  addContactPoint: %f,%f,%f; penetration_depth = %f\n", vertex.x(), vertex.y(), vertex.z(), distance);
                addContactPoint(body1Wrap, body0Wrap,
                    vertex,
                    -plane,
                    distance);
            }
            else
            {
                addContactPoint(body0Wrap, body1Wrap,
                    vertex,
                    plane,
                    distance);
            }
        }
    }

    // printf("Contacts added = %i\n", contactsAdded);

    shape0->unlockChildShapes();

    }
#endif

}

btScalar btSoftPlaneCollisionAlgorithm::calculateTimeOfImpact(btCollisionObject *body0, btCollisionObject *body1, const btDispatcherInfo &dispatchInfo, btManifoldResult *resultOut)
{
    printf("%s\n", "my calculateTimeOfImpact()");
    //not yet
    return btScalar(1.);
}

void btSoftPlaneCollisionAlgorithm::getAllContactManifolds(btManifoldArray &manifoldArray)
{
    //we don't add any manifolds
    printf("%s\n", "my getAllContactManifolds()");
}
